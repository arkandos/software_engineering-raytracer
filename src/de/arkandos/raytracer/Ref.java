package de.arkandos.raytracer;

/**
 * Interior mutability container.
 */
public final class Ref<T> {
    private T value;


    public Ref() {
        value = null;
    }

    public Ref(T initialValue) {
        value = initialValue;
    }


    public T get() {
        return value;
    }


    public Ref<T> set(T newValue) {
        value = newValue;
        return this;
    }


    public boolean hasValue() {
        return (value != null);
    }


    public boolean isEmpty() {
        return (value == null);
    }
}
