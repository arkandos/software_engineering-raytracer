package de.arkandos.raytracer;

public final class RaytracerRenderer implements Camera.Renderer {
    @FunctionalInterface
    public interface Raytracer {
        RaytracerHit cast(Vector origin, Vector ray);
    }


    public final Raytracer Raytracer;


    public RaytracerRenderer(Raytracer raytracer) {
        Raytracer = raytracer;
    }


    public Color renderPixel(int x, int y, Vector origin, Vector ray) {
        var hit = Raytracer.cast(origin, ray);

        if (hit != null) {
            // No real lighting here, just use the angle to the camera, I guess?
            var v1v2 = hit.V1.distanceTo(hit.V2);
            var v1v3 = hit.V1.distanceTo(hit.V3);
            var normal = v1v2.cross(v1v3).normalized();

            var angle = Math.abs(ray.dot(normal));

            return Color.grayscale(angle);
        }
        else {
            return Color.TRANSPARENT;
        }
    }

}
