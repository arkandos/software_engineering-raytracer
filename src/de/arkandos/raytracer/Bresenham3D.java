package de.arkandos.raytracer;

import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Slightly adapted versions of Besenham's line drawing algorithm for the 3d case.
 * See https://stackoverflow.com/questions/16505905/walk-a-line-between-two-points-in-a-3d-voxel-space-visiting-all-cells
 * and http://www.cse.yorku.ca/~amana/research/grid.pdf
 */
public final class Bresenham3D {

    /**
     * This got adapted from
     * http://www.cse.yorku.ca/~amana/research/grid.pdf
     * which seems to work better for Integer start/end points, but fails for floating point ones.
     * So we use iterStartDirection2 for start/end pairs, and our original code for "normal" rays from the camera.
     * This is probably still broken beyond believe, but at least the image looks OK now.....
     *
     * Totally Shippable Kappa
     */
    public static void iterStartDirection2(Vector start, Vector direction, Predicate<Vector> visitor) {
        var startVoxel = start.floor();
        direction = direction.normalized();

        var step = Vector.xyz(
                direction.X >= 0 ? 1 : -1,
                direction.Y >= 0 ? 1 : -1,
                direction.Z >= 0 ? 1 : -1);

        var next = start.distanceTo(startVoxel.add(step));

        var max = Vector.xyz(
                direction.X != 0 ? next.X / direction.X : Float.MAX_VALUE,
                direction.Y != 0 ? next.Y / direction.Y : Float.MAX_VALUE,
                direction.Z != 0 ? next.Z / direction.Z : Float.MAX_VALUE);

        var delta = Vector.xyz(
                direction.X != 0 ? step.X / direction.X : Float.MAX_VALUE,
                direction.Y != 0 ? step.Y / direction.Y : Float.MAX_VALUE,
                direction.Z != 0 ? step.Z / direction.Z : Float.MAX_VALUE);


        var g = startVoxel;
        while(visitor.test(g)) {
            if (max.X < max.Y) {
                if (max.X < max.Z) {
                    g = g.add(step.X, 0f, 0f);
                    max = max.add(delta.X, 0f, 0f);
                } else {
                    g = g.add(0f, 0f, step.Z);
                    max = max.add(0f, 0f, delta.Z);
                }
            } else {
                if (max.Y < max.Z) {
                    g = g.add(0f, step.Y, 0f);
                    max = max.add(0f, delta.Y, 0f);
                } else {
                    g = g.add(0f, 0f, step.Z);
                    max = max.add(0f, 0f, delta.Z);
                }
            }
        }
    }


    /**
     * Iterates through all voxels visited by marching a ray defined by (start, direction)
     * as long as visitor returns true.
     */
    public static void iterStartDirection(Vector start, Vector direction, Predicate<Vector> visitor) {
        //start = start.add(.1f, .1f, .1f);
        var startIdx = start.floor();

        var s = Vector.xyz(
                Math.signum(direction.X),
                Math.signum(direction.Y),
                Math.signum(direction.Z));

        // Planes we will cross next
        var gp = Vector.xyz(
                startIdx.X + (direction.X > 0 ? 1 : 0),
                startIdx.Y + (direction.Y > 0 ? 1 : 0),
                startIdx.Z + (direction.Z > 0 ? 1 : 0));

        // error margins multiplier
        var v = Vector.xyz(
                direction.X == 0 ? 1 : direction.X,
                direction.Y == 0 ? 1 : direction.Y,
                direction.Z == 0 ? 1 : direction.Z);

        var vp = Vector.xyz(
                v.Y * v.Z,
                v.X * v.Z,
                v.X * v.Y);

        // actual errors for the next plane

        var err = start.distanceTo(gp).scale(vp);
        var derr = s.scale(vp);

        var g = startIdx;
        while (visitor.test(g)) {
            var xr = Math.abs(err.X);
            var yr = Math.abs(err.Y);
            var zr = Math.abs(err.Z);

            if (s.X != 0 && (s.Y == 0 || xr < yr) && (s.Z == 0 || xr < zr)) {
                g = g.add(s.X, 0f, 0f);
                err = err.add(derr.X, 0f, 0f);
            } else if (s.Y != 0 && (s.Z == 0 || yr < zr)) {
                g = g.add(0f, s.Y, 0f);
                err = err.add(0f, derr.Y, 0f);
            } else if (s.Z != 0) {
                g = g.add(0f, 0f, s.Z);
                err = err.add(0f, 0f, derr.Z);
            } else {
                // what
                throw new UnsupportedOperationException();
            }
        }
    }

    public static void iterStartEnd(Vector start, Vector end, Consumer<Vector> visitor) {
        var direction  = start.distanceTo(end);

        // start and end are assumed integer pairs already -
        // use version2, because that one works better in that case.
        iterStartDirection2(start, direction, point -> {
            visitor.accept(point);
            return !point.equals(end);
        });
    }

}
