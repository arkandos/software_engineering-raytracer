package de.arkandos.raytracer;

import java.util.Arrays;
import java.util.concurrent.Executors;

/**
 * A simple wrapper around a mutable array allowing for 2-dimensional access on a one-dimensional array,
 * to improve memory locality and increase cache hits, hopefully.
 *
 * This is probably the best we can do in Java, because there is no manual memory management / allocation scheme or something.
 */
@SuppressWarnings("unchecked")
public final class Buffer2D<T> {
    public final T[] Data;
    public final int Width;
    public final int Height;

    @FunctionalInterface
    public interface Visitor<T> {
        void visit(int x, int y, T value);
    }

    @FunctionalInterface
    public interface Supplier<T> {
        T get(T existing, int x, int y);
    }


    public Buffer2D(int width, int height, T zero) {
        Width = width;
        Height = height;

        Data = (T[]) new Object[width * height];
        Arrays.fill(Data, zero);
    }

    public T get(int x, int y) {
        return Data[y * Width + x];
    }


    public Buffer2D<T> set(int x, int y, T value) {
        Data[y * Width + x] = value;
        return this;
    }


    public Buffer2D<T> fill(T zero) {
        Arrays.fill(Data, zero);
        return this;
    }


    public Buffer2D<T> fill(Supplier<T> supplier) {
        for(int y = 0, i = 0; y < Height; ++y) {
            for(int x = 0; x < Width; ++x, ++i) {
                Data[i] = supplier.get(Data[i], x, y);
            }
        }
        return this;
    }


    public void iter(Visitor<T> visitor) {
        for(int y = 0, i = 0; y < Height; ++y) {
            for(int x = 0; x < Width; ++x, ++i) {
                visitor.visit(x, y, Data[i]);
            }
        }
    }


    @Override
    public String toString() {
        return String.format("Buffer2D [%d * %d]", Width, Height);
    }

}
