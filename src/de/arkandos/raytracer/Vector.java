package de.arkandos.raytracer;

import java.util.Objects;

public final class Vector {
    public final float X;
    public final float Y;
    public final float Z;


    public static final Vector ZERO = new Vector(0f, 0f, 0f);
    public static final Vector UP = new Vector(0f, 1f, 0f);
    public static final Vector DOWN = new Vector(0f, -1f, 0f);
    public static final Vector LEFT = new Vector(-1f, 0f, 0f);
    public static final Vector RIGHT = new Vector(1f, 0f, 0f);
    public static final Vector FRONT = new Vector(0f, 0f, -1f);
    public static final Vector BACK = new Vector(0f, 0f, 1f);


    public Vector(float x, float y, float z) {
        X = x;
        Y = y;
        Z = z;
    }

    public static Vector xyz(float x, float y, float z) {
        return new Vector(x, y, z);
    }


    /**
     * The length of the vector.
     */
    public float length() {
        return MathExt.sqrtf(X*X + Y*Y + Z*Z);
    }


    /**
     * A vector in the same direction, but in unit length.
     */
    public Vector normalized() {
        return withLength(1f);
    }


    /**
     * A vector scaled such that result.length() == length.
     */
    public Vector withLength(float length) {
        return scale(length / length());
    }


    /**
     * The addition inverse of the vector.
     * A vector looking in the opposite direction.
     * @return
     */
    public Vector inverse() {
        var result = new Vector(-X, -Y, -Z);
        return result;
    }


    /**
     * The multiplication inverse of the vector.
     * A vector that undos scaling.
     */
    public Vector scaleInverse() {
        var result = new Vector(1f/X, 1f/Y, 1f/Z);
        return result;
    }


    /**
     * The length, squared.
     * This is faster to compute than the length itself, and squaring it.
     */
    public float sqLength() {
        return (X*X + Y*Y + Z*Z);
    }


    /**
     * The dot product between this vector and another one.
     */
    public float dot(Vector v2) {
        return (X*v2.X + Y*v2.Y + Z*v2.Z);
    }


    /**
     * The vector product between this vector and another one.
     */
    public Vector cross(Vector v2) {
        var result = new Vector(
                Y * v2.Z - Z * v2.Y,
                Z * v2.X - X * v2.Z,
                X * v2.Y - Y * v2.X
        );
        return result;
    }


    /**
     * Add 2 vectors.
     */
    public Vector add(Vector v2) {
        var result = new Vector(X + v2.X, Y + v2.Y, Z + v2.Z);
        return result;
    }

    /**
     * Add 2 vectors, but scale the second one before adding it.
     * Equivalent to this.add(v2.scale(scale))
     */
    public Vector add(float scale, Vector v2) {
        var result = new Vector(X + v2.X * scale, Y + v2.Y * scale, Z + v2.Z * scale);
        return result;
    }

    /**
     * Add a vector without constructing one.
     */
    public Vector add(float x, float y, float z) {
        var result = new Vector(X + x, Y + y, Z + z);
        return result;
    }


    /**
     * A vector that is the direction to go to v2, such that
     * this.add(this.distanceTo(v2)) == v2
     */
    public Vector distanceTo(Vector v2) {
        var result = new Vector(v2.X - X, v2.Y - Y, v2.Z - Z);
        return result;
    }


    /**
     * Uniformly scaled version of this vector.
     */
    public Vector scale(float scale) {
        var result = new Vector(X * scale, Y * scale, Z * scale);
        return result;
    }

    /**
     * Scaled version of this vector, where every component is scaled using the components of another vector.
     */
    public Vector scale(Vector scale) {
        var result = new Vector(X * scale.X, Y * scale.Y, Z * scale.Z);
        return result;
    }


    /**
     * A vector with all its components rounded down.
     */
    public Vector floor() {
        var result = new Vector(MathExt.floorf(X), MathExt.floorf(Y), MathExt.floorf(Z));
        return result;
    }


    public boolean equals(Vector v2) {
        return (X == v2.X && Y == v2.Y && Z == v2.Z);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof Vector) {
            return equals((Vector) o);
        } else {
            return false;
        }
    }


    @Override
    public int hashCode() {
        return Objects.hash(X, Y, Z);
    }


    @Override
    public String toString() {
        return String.format("(%g; %g; %g)", X, Y, Z);
    }
}
