package de.arkandos.raytracer;

public final class MathExt {
    public static float sinf(float r) {
        return (float) Math.sin(r);
    }

    public static float cosf(float r) {
        return (float) Math.cos(r);
    }

    public static float tanf(float r) {
        return (float) Math.tan(r);
    }

    public static float asinf(float s) {
        return (float) Math.asin(s);
    }

    public static float acosf(float c) {
        return (float) Math.acos(c);
    }

    public static float atanf(float t) {
        return (float) Math.atan(t);
    }

    public static float atan2f(float s, float c) {
        return (float) Math.atan2(s, c);
    }

    public static float sqrtf(float v) {
        return (float) Math.sqrt(v);
    }

    public static float floorf(float v) {
        return (float) Math.floor(v);
    }

    public static boolean approximatelyZero(float x) {
        return (x < 0.0000001f && x > -0.0000001f);
    }

    public static float deg2rad(float deg) {
        return (float)(deg * Math.PI / 180);
    }

    public static float rad2deg(float rad) {
        return (float)(rad * 180 / Math.PI);
    }

    public static int clamp(int v, int min, int max) {
        if (v < min) return min;
        else if (v > max) return max;
        else return v;
    }

    public static float clamp(float v, float min, float max) {
        if (v < min) return min;
        else if (v > max) return max;
        else return v;
    }
}
