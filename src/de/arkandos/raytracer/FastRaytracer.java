package de.arkandos.raytracer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public final class FastRaytracer implements RaytracerRenderer.Raytracer {
    public final static class MeshData {
        public final Mesh Object;
        public final float WorldToHashScale;
        public final HashMap<Vector, ArrayList<Vector>> SpatialHash;
        public final BoundingBox HashBounds;

        public MeshData(Mesh object) {
            Object = object;

            var triangleBoundingBox = object.foldTriangles(BoundingBox.EMPTY, (v1, v2, v3, state) -> {
                var boundingBox = BoundingBox.ofPoints(v1, v2, v3);
                return state.extendRadius(boundingBox);
            });

            var boxSize = triangleBoundingBox.sphereRadius();

            // scale the bounding box a little bit to make sure,
            // then 1/x all values to invert the scale.

            WorldToHashScale = 1f / boxSize;

            HashBounds = BoundingBox.ofMinMax(
                    hashedCoords(object.Bounds.min()),
                    hashedCoords(object.Bounds.max()));

            SpatialHash = new HashMap<>();
            object.iterTriangles((v1, v2, v3) -> {
                var v1h = hashedCoords(v1);
                var v2h = hashedCoords(v2);
                var v3h = hashedCoords(v3);

                var hashCoords = new HashSet<Vector>();
                Bresenham3D.iterStartEnd(v1h, v2h, hashCoords::add);
                Bresenham3D.iterStartEnd(v1h, v3h, hashCoords::add);
                Bresenham3D.iterStartEnd(v2h, v3h, hashCoords::add);

                for(Vector coord : hashCoords) {
                    var triangleList = SpatialHash.getOrDefault(coord, new ArrayList<Vector>());
                    triangleList.add(v1);
                    triangleList.add(v2);
                    triangleList.add(v3);
                    SpatialHash.put(coord, triangleList);
                }
            });
        }


        public Vector hashedCoords(Vector v) {
            var result = v.scale(WorldToHashScale).floor();
            return result;
        }


        public Vector scale(Vector v) {
            var result = v.scale(WorldToHashScale);
            return result;
        }


        public <T> T foldTrianglesOfBucket(Vector bucket, T state, Mesh.TriangleFolder<T> folder) {
            var triangles = SpatialHash.getOrDefault(bucket, null);

            if (triangles != null) {
                for(int i = 0; i < triangles.size(); i += 3) {
                    var v1 = triangles.get(i + 0);
                    var v2 = triangles.get(i + 1);
                    var v3 = triangles.get(i + 2);

                    state = folder.transform(v1, v2, v3, state);
                }
            }

            return state;
        }
    }


    public final MeshData[] Objects;


    public FastRaytracer(Mesh... objects) {
        Objects = Arrays.stream(objects).map(MeshData::new).toArray(MeshData[]::new);
    }


    public RaytracerHit cast(Vector origin, Vector ray) {
        RaytracerHit hit = null;

        for(int objectIdx = 0; objectIdx < Objects.length; ++objectIdx) {
            var object = Objects[objectIdx];

            var inverseModelTransform = object.Object.ModelToWorld.inverse();
            var objectOrigin = inverseModelTransform.applyToPoint(origin);
            var objectRay = inverseModelTransform.applyToDirection(ray);

            var hashedOrigin = object.scale(objectOrigin);
            var hashedDirection = object.scale(objectRay);

            var checkBounds = object.HashBounds.extend(BoundingBox.ofPoint(hashedOrigin.floor()));

            var objectHit = new Ref<RaytracerHit>();
            Bresenham3D.iterStartDirection(hashedOrigin, hashedDirection, point -> {
                // exit condition: no longer inside the bounding box.
                if (!checkBounds.contains(point))
                    return false;

                RaytracerHit bucketHit = object.foldTrianglesOfBucket(point, objectHit.get(), (v1, v2, v3, hit2) -> {
                    var intersection = MoellerTrumbore.rayTriangleIntersectionPoint(objectOrigin, objectRay, v1, v2, v3);
                    if (intersection != null) {
                        var transformedIntersection =  object.Object.ModelToWorld.applyToPoint(intersection);
                        if (hit2 == null || transformedIntersection.sqLength() < hit2.Hit.sqLength()) {
                            return new RaytracerHit(object.Object,
                                    transformedIntersection,
                                    object.Object.ModelToWorld.applyToPoint(v1),
                                    object.Object.ModelToWorld.applyToPoint(v2),
                                    object.Object.ModelToWorld.applyToPoint(v3));
                        } else {
                            return hit2;
                        }
                    } else {
                        return hit2;
                    }
                });

                if (bucketHit != null) {
                    // can exit as soon as we found a fit in the bucket - this is guaranteed to be the nearest,
                    // because we walk buckets in the right order.
                    objectHit.set(bucketHit);
                    return false;
                } else {
                    return true;
                }
            });

            if (objectHit.hasValue() && (hit == null || objectHit.get().Hit.sqLength() < hit.Hit.sqLength())) {
                hit = objectHit.get();
            }
        }

        return hit;
    }
}
