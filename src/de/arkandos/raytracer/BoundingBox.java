package de.arkandos.raytracer;

public final class BoundingBox {
    public final Vector Center;
    public final Vector Radius;


    public static final BoundingBox EMPTY = ofCenterRadius(Vector.ZERO, Vector.ZERO);


    private BoundingBox(Vector center, Vector radius) {
        Center = center;
        Radius = radius;
    }


    /**
     * Returns a new Bounding Box around center, which extend by radius in each direction, respectively.
     */
    public static BoundingBox ofCenterRadius(Vector center, Vector radius) {
        return new BoundingBox(center, radius);
    }


    /**
     * Returns a bounding box including 2 points-
     */
    public static BoundingBox ofMinMax(Vector min, Vector max) {
        var radius = min.distanceTo(max).scale(.5f);
        var center = min.add(radius);

        return new BoundingBox(center, radius);
    }


    /**
     * Returns a zero-size bounding box for a single point.
     */
    public static BoundingBox ofPoint(Vector point) {
        return ofCenterRadius(point, Vector.ZERO);
    }


    /**
     * Given a list of points, construct a bounding box big enough so that all points are inside of it.
     * Or that result.contains(p) forall p: points.
     */
    public static BoundingBox ofPoints(Vector... points) {
        float minx = Float.MAX_VALUE;
        float miny = Float.MAX_VALUE;
        float minz = Float.MAX_VALUE;
        float maxx = -Float.MAX_VALUE;
        float maxy = -Float.MAX_VALUE;
        float maxz = -Float.MAX_VALUE;

        for(int i = 0; i < points.length; ++i) {
            var p = points[i];
            minx = p.X < minx ? p.X : minx;
            maxx = p.X >= maxx ? p.X : maxx;

            miny = p.Y < miny ? p.Y : miny;
            maxy = p.Y >= maxy ? p.Y : maxy;

            minz = p.Z < minz ? p.Z : minz;
            maxz = p.Z >= maxz ? p.Z : maxz;
        }

        return ofMinMax(new Vector(minx, miny, minz), new Vector(maxx, maxy, maxz));
    }


    /**
     * The diameter of the box, which is twice its radius
     */
    public Vector diameter() {
        return Radius.scale(2);
    }


    /**
     * The radius of the sphere that would include this bounding box.
     */
    public float sphereRadius() {
        var radius = Math.max(Math.max(Radius.X, Radius.Y), Radius.Z);
        return radius;
    }


    /**
     * The "min" vector of this bounding box,
     * or the left-back-down corner.
     */
    public Vector min() {
        return Center.add(-1f, Radius);
    }


    /**
     * The "max" vector of this bounding box,
     * or the right-top-front corner.
     */
    public Vector max() {
        return Center.add(Radius);
    }


    /**
     * A boolean indicating whether or not a given point is inside the box.
     */
    public boolean contains(Vector point) {
        var d = Center.distanceTo(point);

        var result = (
                Math.abs(d.X) <= Radius.X &&
                Math.abs(d.Y) <= Radius.Y &&
                Math.abs(d.Z) <= Radius.Z);

        return result;
    }


    /**
     * Extends the radius in each direction to include both BoundingBoxes (e.g. use the max extend in each direction),
     * and return a new BoundingBox using that radius, and the averaged center of both boxes.
     *
     * This does not create a bounding box that includes either of the original ones,
     * only a bounding box whose radius is big enough.
     */
    public BoundingBox extendRadius(BoundingBox other) {
        var joinedRadius= Vector.xyz(
                Math.max(Radius.X, other.Radius.X),
                Math.max(Radius.Y, other.Radius.Y),
                Math.max(Radius.Z, other.Radius.Z));

        var joinedCenter = Center.add(other.Center).scale(0.5f);

        var result = BoundingBox.ofCenterRadius(joinedCenter, joinedRadius);

        return result;
    }


    /**
     * Returns a new BoundingBox that is big enough to include both bounding boxes inside of it.
     */
    public BoundingBox extend(BoundingBox other) {
        var thisMin = min();
        var thisMax = max();
        var otherMin = other.min();
        var otherMax = other.max();

        var min = Vector.xyz(
                Math.min(thisMin.X, otherMin.X),
                Math.min(thisMin.Y, otherMin.Y),
                Math.min(thisMin.Z, otherMin.Z));

        var max = Vector.xyz(
                Math.max(thisMax.X, otherMax.X),
                Math.max(thisMax.Y, otherMax.Y),
                Math.max(thisMax.Z, otherMax.Z));

        return BoundingBox.ofMinMax(min, max);
    }
}
