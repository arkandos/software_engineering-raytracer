package de.arkandos.raytracer;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public final class Mesh {
    @FunctionalInterface
    public interface TriangleConsumer {
        public void accept(Vector v1, Vector v2, Vector v3);
    }

    @FunctionalInterface
    public interface TriangleFolder<T> {
        T transform(Vector v1, Vector v2, Vector v3, T state);
    }


    public final Vector[] Vertices;
    public final int[] Triangles;
    public final BoundingBox Bounds;
    public final Transform ModelToWorld;

    private Mesh(Vector[] vertices, int[] triangles, BoundingBox bounds, Transform modelToWorld) {
        Vertices = vertices;
        Triangles = triangles;
        Bounds = bounds;
        ModelToWorld = modelToWorld;
    }

    public static Mesh loadResourceObj(String name) {
        var stream = ClassLoader.getSystemResourceAsStream(name);
        if (stream != null) {
            return loadObj(stream);
        } else {
            return null;
        }
    }

    /**
     * Load a scene object from an .obj file.
     * This is a very primitive and uncomplete routine that only supports exactly as much as we need to read
     * the "bunny.obj" file. There is also no proper handling of parsing errors, and it uses java.util.Scanner...
     * You have been warned!
     */
    public static Mesh loadObj(InputStream inputStream) {
        try (var scanner = new Scanner(new InputStreamReader(inputStream))) {
            scanner.useLocale(Locale.ROOT);

            var vertices = new ArrayList<Vector>();
            var triangles = new ArrayList<Integer>();

            while (scanner.hasNext()) {
                switch (scanner.next().charAt(0)) {
                    case '#':
                        scanner.nextLine();
                        break; // # skip comments.
                    case 'v':
                        vertices.add(new Vector(
                                scanner.nextFloat(),
                                scanner.nextFloat(),
                                scanner.nextFloat()));
                        break;
                    case 'f':
                        // triangles are 1-based
                        triangles.add(scanner.nextInt() - 1);
                        triangles.add(scanner.nextInt() - 1);
                        triangles.add(scanner.nextInt() - 1);
                        break;
                }
            }

            var verticesArray = vertices.toArray(new Vector[vertices.size()]);
            var trianglesArray = triangles.stream().mapToInt(Integer::valueOf).toArray();

            var bounds = BoundingBox.ofPoints(verticesArray);

            // put model at the origin by default.
            var modelToWorld = Transform.translation(bounds.Center.inverse());

            var obj = new Mesh(verticesArray, trianglesArray, bounds, modelToWorld);

            return obj;
        }
    }


    /**
     * The position of this Mesh in world coordinates.
     */
    public Vector position() {
        return ModelToWorld.applyToPoint(Bounds.Center);
    }


    /**
     * The bounds of this mesh in world coordinates.
     */
    public BoundingBox worldBounds() {
        var worldBounds = BoundingBox.ofCenterRadius(
                ModelToWorld.applyToPoint(Bounds.Center),
                ModelToWorld.applyToDirection(Bounds.Radius));

        return worldBounds;
    }


    /**
     * Return this mesh using a different transform.
     */
    public Mesh withTransform(Transform newModelToWorld) {
        var result = new Mesh(
                Vertices,
                Triangles,
                Bounds,
                newModelToWorld
        );

        return result;
    }


    /**
     * Move this mesh relatively to its current position.
     */
    public Mesh translate(Vector translate) {
        return withTransform(ModelToWorld.translate(translate));
    }


    /**
     * Loop over all triangles.
     */
    public void iterTriangles(TriangleConsumer consumer) {
        for(int i = 0; i < Triangles.length; i += 3) {
            consumer.accept(
                    Vertices[Triangles[i + 0]],
                    Vertices[Triangles[i + 1]],
                    Vertices[Triangles[i + 2]]
            );
        }
    }


    /**
     * Loop over all triangles, passing a state around!
     */
    public <T> T foldTriangles(T state, TriangleFolder<T> folder) {
        for(int i = 0; i < Triangles.length; i += 3) {
            state = folder.transform(
                    Vertices[Triangles[i + 0]],
                    Vertices[Triangles[i + 1]],
                    Vertices[Triangles[i + 2]],
                    state
            );
        }
        return state;
    }
}
