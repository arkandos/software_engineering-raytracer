package de.arkandos.raytracer;

public final class SlowRaytracer implements RaytracerRenderer.Raytracer {

    public final Mesh[] Objects;

    // Objects are stable
    public SlowRaytracer(Mesh... objects) {
        Objects = objects;
    }

    public RaytracerHit cast(Vector origin, Vector ray) {
        RaytracerHit hit = null;

        for(int objectIdx = 0; objectIdx < Objects.length; ++objectIdx) {
            var object = Objects[objectIdx];
            var inverseModelTransform = object.ModelToWorld.inverse();
            var objectOrigin = inverseModelTransform.applyToPoint(origin);
            var objectRay = inverseModelTransform.applyToDirection(ray);

            hit = object.foldTriangles(hit, (v1, v2, v3, hit2) -> {
                var intersection = MoellerTrumbore.rayTriangleIntersectionPoint(objectOrigin, objectRay, v1, v2, v3);

                if (intersection != null) {
                    var transformedIntersection = object.ModelToWorld.applyToPoint(intersection);
                    if (hit2 == null || transformedIntersection.sqLength() < hit2.Hit.sqLength()){
                        return new RaytracerHit(object,
                                transformedIntersection,
                                object.ModelToWorld.applyToPoint(v1),
                                object.ModelToWorld.applyToPoint(v2),
                                object.ModelToWorld.applyToPoint(v3));
                    } else {
                        return hit2;
                    }
                } else {
                    return hit2;
                }
            });
        }

        return hit;
    }
}

