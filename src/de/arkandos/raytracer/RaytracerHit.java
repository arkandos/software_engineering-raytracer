package de.arkandos.raytracer;

import java.util.Objects;

public final class RaytracerHit {
    public final Mesh Object;
    public final Vector Hit;
    public final Vector V1;
    public final Vector V2;
    public final Vector V3;

    public RaytracerHit(Mesh object, Vector hit, Vector v1, Vector v2, Vector v3) {
        Object = object;
        Hit = hit;
        V1 = v1;
        V2 = v2;
        V3 = v3;
    }


    public boolean equals(RaytracerHit hit2) {
        var hit = this;
        return (hit.Object.equals(hit2.Object) && hit.Hit.equals(hit2.Hit));
    }


    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (o instanceof RaytracerHit) {
            return equals((RaytracerHit) o);
        } else {
            return false;
        }
    }


    @Override public int hashCode() {
        return Objects.hash(Object, Hit);
    }


    @Override
    public String toString() {
        return String.format("RaytracerHit %s @ %s", Object.toString(), Hit.toString());
    }
}
