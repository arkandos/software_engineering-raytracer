package de.arkandos.raytracer;

public final class Program {
    public static void main(String[] args) {
        var bunnyObj = Mesh.loadResourceObj("bunny.obj");

        var cameraDirection = Vector.xyz(0.3f, -0.3f, -0.2f);
        var cameraDistance = 5 * bunnyObj.Bounds.sphereRadius();

        var camera = Camera.aspectScaled(
                Color.rgb(0f, 0.2f, 0.2f),
                Transform.lookAtFromDirection(cameraDirection, cameraDistance, bunnyObj.position()),
                800,
                60f,
                16f / 9f);

        var slowRenderer = new RaytracerRenderer(new SlowRaytracer(bunnyObj));
        var fastRenderer = new RaytracerRenderer(new FastRaytracer(bunnyObj));

        benchmark("Fast Render", () -> camera.render(fastRenderer));
        camera.intoFile("bunny_fast.png");

        // benchmark("Slow Render", () -> camera.render(slowRenderer));
        // camera.intoFile("bunny_slow.png");
    }


    public static void benchmark(String name, Runnable action) {
        System.out.println(String.format("Start %s ...", name));

        var startTime = System.nanoTime();

        try {
            action.run();
        } catch(Exception e) {
            System.err.println(String.format("ERROR running %s:\n%s\n", name, e.toString()));
        }

        var elapsed = System.nanoTime() - startTime;
        System.out.println(String.format("Finished %s in %d ms", name, elapsed / 1000000));
    }
}
