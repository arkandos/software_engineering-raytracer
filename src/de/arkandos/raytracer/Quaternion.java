package de.arkandos.raytracer;

import java.util.Objects;

/**
 * Rotations are represented as Quaternions.
 */
public final class Quaternion {
    public final float W;
    public final float X;
    public final float Y;
    public final float Z;


    public static final Quaternion IDENTITY = new Quaternion(1f, 0f, 0f, 0f);
    public static final Quaternion ZERO = new Quaternion(0f, 0f, 0f, 0f);


    public Quaternion(float w, float x, float y, float z) {
        W = w;
        X = x;
        Y = y;
        Z = z;
    }

    /**
     * Make a Quaternion from an angle around an axis.
     */
    public static Quaternion ofAxisAngle(float angle, Vector axis) {
        var normAxis = axis.normalized();
        var sin = MathExt.sinf(angle * .5f);
        var cos = MathExt.cosf(angle * .5f);

        var rotation = new Quaternion(cos, sin * normAxis.X, sin * normAxis.Y, sin * normAxis.Z);

        return rotation;
    }

    /**
     * Make a rotation from a triplet of euler angles representing rotations around the X/Y/Z - axis.
     * See https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
     */
    public static Quaternion ofEuler(float pitch, float roll, float yaw) {
        var sinPitch = Math.sin(pitch * .5f);
        var cosPitch = Math.cos(pitch * .5f);
        var sinRoll = Math.sin(roll * .5f);
        var cosRoll = Math.cos(roll * .5f);
        var sinYaw = Math.sin(yaw * .5f);
        var cosYaw = Math.cos(yaw * .5f);

        var cosRollXcosYaw = cosRoll * cosYaw;
        var sinRollXsinYaw = sinRoll * sinYaw;
        var cosRollXsinYaw = cosRoll * sinYaw;
        var sinRollXcosYaw = sinRoll * cosYaw;

        var rotation = new Quaternion(
            (float)(cosPitch * cosRollXcosYaw - sinPitch * sinRollXsinYaw),
            (float)(sinPitch * cosRollXcosYaw + cosPitch * sinRollXsinYaw),
            (float)(cosPitch * sinRollXcosYaw - sinPitch * cosRollXsinYaw),
            (float)(cosPitch * cosRollXsinYaw + sinPitch * sinRollXcosYaw));

        return rotation;
    }


    /**
     * Create a quaternion representing the rotation between 2 vectors.
     * See http://lolengine.net/blog/2013/09/18/beautiful-maths-quaternion-from-vectors
     */
    public static Quaternion ofVectors(Vector from, Vector to) {
        var combinedLengths = MathExt.sqrtf(from.sqLength() * to.sqLength());
        var cosTheta = from.dot(to);

        // cross product is 0 -> rotation of 180deg around an arbitrary axis.
        var oneEighty = (combinedLengths + cosTheta < 1e-6f * combinedLengths);

        var xyz = oneEighty
                ? Math.abs(from.X) > Math.abs(from.Z)
                        ? new Vector(-from.Y, from.X, 0f)
                        : new Vector(0f, -from.Z, from.Y)
                : from.cross(to);

        var w = (oneEighty ? 0 : combinedLengths + cosTheta);

        var norm = MathExt.sqrtf(xyz.sqLength() + w*w);

        var rotation = new Quaternion(
            w / norm,
            xyz.X / norm,
            xyz.Y / norm,
            xyz.Z / norm
        );

        return rotation;
    }


    /**
     * Because the Quaternion is assumed to be unit length, the inverse is its conjugate.
     */
    public Quaternion inverse() {
        var conjugate = new Quaternion(W, -X, -Y, -Z);
        return conjugate;
    }


    /**
     * Multiplies 2 quaternions.
     * See: http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/code/index.htm
     */
    public Quaternion mul(Quaternion q) {
        var w = W*q.W - X*q.X - Y*q.Y - Z*q.Z;
        var x = X*q.W + W*q.X + Y*q.Z - Z*q.Y;
        var y = Y*q.W + W*q.Y + Z*q.X - X*q.Z;
        var z = Z*q.W + W*q.Z + X*q.Y - Y*q.X;

        var norm = MathExt.sqrtf(w*w + x*x + y*y + z*z);

        var result = new Quaternion(
                w / norm,
                x / norm,
                y / norm,
                z / norm);

        return result;
    }


    /**
     * Apply this rotation to a Vector v, returning a new rotated vector.
     */
    public Vector apply(Vector v) {
        // quaternion * vector
        var ix = W * v.X + Y * v.Z - Z * v.Y;
        var iy = W * v.Y + Z * v.X - X * v.Z;
        var iz = W * v.Z + X * v.Y - Y * v.X;
        var iw = - X * v.X - Y * v.Y - Z * v.Z;

        // result * inverse quaternion
        var result = new Vector(
                ix * W + iw * -X + iy * -Z - iz * -Y,
                iy * W + iw * -Y + iz * -X - ix * -Z,
                iz * W + iw * -Z + ix * -Y - iy * -X
        );

        return result;
    }


    /**
     * Converts the quaternion back into euler angles.
     * See https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
     */
    public Vector euler() {
        var euler = new Vector(
                MathExt.atan2f(2 * (W * X + Y * Z), 1 - 2 * (X * X + Y * Y)),
                MathExt.asinf(MathExt.clamp(2 * (W * Y - Z * X), -1f, 1f)),
                MathExt.atan2f(2 * (W * Z + X * Y), 1 - 2 * (Y * Y + Z * Z)));

        return euler;
    }


    public boolean equals(Quaternion q) {
        return (W == q.W && X == q.X && Y == q.Y && Z == q.Z);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof Quaternion) {
            return equals((Quaternion) o);
        } else {
            return false;
        }
    }


    @Override
    public int hashCode() {
        return Objects.hash(W, X, Y, Z);
    }


    @Override
    public String toString() {
        // return String.format("(%g + %gi + %gj + %gk)", W, X, Y, Z);
        var euler = this.euler();
        return String.format("(%g°; %g°; %g°)", MathExt.rad2deg(euler.X), MathExt.rad2deg(euler.Y), MathExt.rad2deg(euler.Z));
    }
}
