package de.arkandos.raytracer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Camera {
    public final Color ClearColor;
    public final Transform CameraToWorld;

    // perspective projections only, sorry.
    public final float HorizontalFOV;
    public final float VerticalFOV;

    // Buffers are mutable, so we don't expose them!
    private final Buffer2D<Color> framebuffer;



    @FunctionalInterface
    public interface Renderer {
        Color renderPixel(int x, int y, Vector origin, Vector ray);
    }



    private Camera(Color clearColor, Transform transform, int width, int height, float horizontalFOV, float verticalFOV) {
        ClearColor = clearColor;
        CameraToWorld = transform;
        HorizontalFOV = horizontalFOV;
        VerticalFOV = verticalFOV;

        framebuffer = new Buffer2D<>(width, height, clearColor);
    }

    /**
     * Construct a camera using an aspectRatio to scale both the height of the display, as well as the horizontal field of view.
     * This uses the Hor+ method of fixing the vertical aspect ratio to "better" support wide display modes.
     * @param clearColor Color to clear all pixel buffers to - effectively the background color
     * @param worldToCamera Transform from World to Camera - Space, usually constructed by Transform.lookAt to emulate a
     *                      camera standing in the room at a certain position.
     * @param width Width, in pixels, of the underlying buffer.
     * @param fov   Vertical field of view in degrees! Good values are in the 50-120 range.
     * @param aspectRatio Aspect Ratio of the display, e.g. 16/9
     */
    public static Camera aspectScaled(Color clearColor, Transform worldToCamera, int width, float fov, float aspectRatio) {
        var height = (int) (width / aspectRatio);
        var verticalFOV = MathExt.deg2rad(fov);
        var horizontalFOV = 2.0 * Math.atan(aspectRatio * Math.tan(verticalFOV / 2.0));

        var camera = new Camera(clearColor, worldToCamera.inverse(), width, height, (float)horizontalFOV, (float)verticalFOV);

        return camera;
    }


    /**
     * Clears all backing buffers using the ClearColor.
     */
    public Camera clear() {
        framebuffer.fill(ClearColor);
        return this;
    }


    /**
     * Using a renderer, fill all the pixels!
     * The renderer gets passed the integer coordinates of the pixel as well as a direction and a origin of the camera ray,
     * used to describe the "perspectiveness" of the camera.
     *
     * The main reason for this is because its quite handy if you build a raytracer!
     *
     * But we still use Transforms under the hood, which means it should be technically adaptable to other rendering techniques as well.
     */
    public Camera render(Renderer supplier) {
        var halfW = framebuffer.Width / 2f - .5f;
        var halfH = framebuffer.Height / 2f - .5f;

        var dH = Vector.RIGHT.scale(MathExt.tanf(HorizontalFOV / 2f) / framebuffer.Width);
        var dV = Vector.UP.scale(-1 * MathExt.tanf(VerticalFOV / 2f) / framebuffer.Height);

        var origin = CameraToWorld.applyToPoint(Vector.ZERO);

        framebuffer.fill((c, x, y) -> {
            var ray = CameraToWorld.applyToDirection(Vector.FRONT.add(x - halfW, dH).add(y - halfH, dV));
            var color = supplier.renderPixel(x, y, origin, ray);
            return ClearColor.blendInto(color);
        });
        return this;
    }


    /**
     * Extracts the pixel's color values from the underlying buffers and writes them into an awt Image.
     */
    public BufferedImage intoImage() {
        var image = new BufferedImage(framebuffer.Width, framebuffer.Height, BufferedImage.TYPE_INT_ARGB);
        framebuffer.iter((x, y, color) -> image.setRGB(x, y, color.argb()));
        return image;
    }


    /**
     * Writes the color buffer to a PNG file.
     */
    public Camera intoFile(String fileName) {
        var image = intoImage();

        try {
            ImageIO.write(image, "PNG", new File(fileName));
        } catch (IOException e) {
            System.err.println(String.format("Error writing file:\n%s\n", e.toString()));
        }

        return this;
    }
}
