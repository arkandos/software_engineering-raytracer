package de.arkandos.raytracer;

public final class MoellerTrumbore {
    /**
     * Check if a ray (origin, ray) hits a triangle (v1, v2, v3).
     * Backface-Culling is used to only check triangles that are facing the ray.
     *
     * Returns the intersection point on the triangle if they both collide,
     * or null if there is no intersection.
     *
     * See http://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/moller-trumbore-ray-triangle-intersection
     */
    public static Vector rayTriangleIntersectionPoint(Vector origin, Vector ray, Vector v1, Vector v2, Vector v3) {
        var v1v2 = v1.distanceTo(v2);
        var v1v3 = v1.distanceTo(v3);
        var normal = ray.cross(v1v3);
        var det = v1v2.dot(normal);

        // culling: if the triangle is back-facing, we don't intersect it.
        if (det < 0.0000001) {
            // if (MathExt.approximatelyZero(det)) {

            return null;
        }

        var invDet = 1f / det;

        var tvec = v1.distanceTo(origin);
        var u = invDet * tvec.dot(normal);

        if (u < 0f || u > 1f) {
            return null;
        }

        var qvec = tvec.cross(v1v2);
        var v = invDet * ray.dot(qvec);

        if (v < 0f || u + v > 1f) {
            return null;
        }

        var t = invDet * v1v3.dot(qvec);
        if (t > 0.0000001) {
            return ray.scale(t);
        }

        return null;
    }
}
