package de.arkandos.raytracer;

import java.util.Objects;

public final class Color {
    public final float R;
    public final float G;
    public final float B;
    public final float A;


    public static final Color TRANSPARENT = new Color(0f, 0f, 0f, 0f);
    public static final Color BLACK = new Color(0f, 0f, 0f, 1f);
    public static final Color WHITE = new Color(1f, 1f, 1f, 1f);
    public static final Color RED = new Color(1f, 0f, 0f, 1f);
    public static final Color GREEN = new Color(0f, 1f, 0f, 1f);
    public static final Color BLUE = new Color(0f, 0f, 1f, 1f);


    private Color(float r, float g, float b, float a) {
        R = r;
        G = g;
        B = b;
        A  = MathExt.clamp(a, 0f, 1f);
    }

    public static Color rgba(float r, float g, float b, float a) {
        var color = new Color(r, g, b, a);
        return color;
    }

    public static Color rgb(float r, float g, float b) {
        var color = new Color(r, g, b, 1.0f);
        return color;
    }

    public static Color rgba(int r, int g, int b, int a) {
        var color = new Color(r/255.0f, g/255.0f, b/255.0f, a/255.0f);
        return color;
    }

    public static Color rgb(int r, int g, int b) {
        var color = new Color(r/255.0f, g/255.0f, b/255.0f, 1.0f);
        return color;
    }


    public static Color grayscale(float gray) {
        return rgb(gray, gray, gray);
    }

    public static Color grayscale(int gray) {
        return rgb(gray, gray, gray);
    }


    /**
     * Constructs a color from a ARGB integer value.
     */
    public static Color hex(int color) {
        return rgba((color >> 16) & 0xFF, (color >> 8) & 0xFF, (color >> 0) & 0xFF, color >> 24);
    }


    public Color withAlpha(float newAlpha) {
        return new Color(R, G, B, newAlpha);
    }


    /**
     * Returns the red channel value scaled to [0-255]
     */
    public int Ri() {
        return MathExt.clamp((int)(R * 255), 0, 255);
    }


    /**
     * Returns the green channel value scaled to [0-255]
     */
    public int Gi() {
        return MathExt.clamp((int)(G * 255), 0, 255);
    }


    /**
     * Returns the blue channel value scaled to [0-255]
     */
    public int Bi() {
        return MathExt.clamp((int)(B * 255), 0, 255);
    }


    /**
     * Returns the alpha channel value scaled to [0-255]
     */
    public int Ai() {
        return MathExt.clamp((int)(A * 255), 0, 255);
    }


    /**
     * Returns an integer representing this color in ARGB format.
     * @return
     */
    public int argb() {
        var argb = ((Ai() << 24) | (Ri() << 16) | (Gi() << 8) | (Bi() << 0));
        return argb;
    }


    /**
     * Alpha-Blend another color onto this one, such that this color becomes the background.
     */
    public Color blendInto(Color newForegroundColor) {
        var a = newForegroundColor.A;
        var na = 1f - a;

        var outA = a + A * na;

        var out = Color.rgba(
                (a * newForegroundColor.R + R * A * na) / outA,
                (a * newForegroundColor.G + G * A * na) / outA,
                (a * newForegroundColor.B + B * A * na) / outA,
                outA
        );

        return out;
    }


    public boolean equals(Color c2) {
        var c1 = this;
        return (c1.R == c2.R && c1.G == c2.G && c1.B == c2.B && c1.A == c2.A);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (o instanceof Color) {
            return equals((Color) o);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return String.format("#%02x%02x%02x%02x", Ri(), Gi(), Bi(), Ai());
    }

    @Override
    public int hashCode() {
        return Objects.hash(R, G, B, A);
    }

}
