package de.arkandos.raytracer;

/**
 * Transforms are a combination of a rotation and a translation.
 * We don't use a Matrix here, because we don't need support for scaling,
 * and don't want to deal with 4D-Vectors, or projections, or anything of that kind that would require
 * "real" 3D+1 math.
 */
public final class Transform {
    public final Vector Translation;
    public final Quaternion Rotation;


    public static final Transform IDENTITY = new Transform(Vector.ZERO, Quaternion.IDENTITY);


    private Transform(Vector translation, Quaternion rotation) {
        Translation = translation;
        Rotation = rotation;
    }

    /**
     * Creates a transform that looks from from to at.
     */
    public static Transform lookAt(Vector from, Vector at, Vector up) {
        up = up.normalized();
        var direction  = from.distanceTo(at).normalized();


        Quaternion rotation;
        if (!up.equals(direction)) {
            // see https://answers.unity.com/questions/819699/calculate-quaternionlookrotation-manually.html
            var v = direction.add(up.scale(-up.dot(direction)));
            var q = Quaternion.ofVectors(Vector.FRONT, v);
            rotation = Quaternion.ofVectors(v, direction).mul(q).inverse();
        } else {
            rotation = Quaternion.ofVectors(direction, Vector.FRONT);
        }

        var translation = rotation.apply(from.inverse());

        var transform = new Transform(translation, rotation);

        return transform;
    }

    public static Transform lookAt(Vector from, Vector at) {
        return lookAt(from, at, Vector.UP);
    }


    public static Transform lookAtFromDirection(Vector direction, float distance, Vector at) {
        var from = at.add(-1f, direction.withLength(distance));
        return lookAt(from, at);
    }


    /**
     * Returns a Transform only representing a translation by the given vector.
     */
    public static Transform translation(Vector t) {
        var transform = new Transform(t, Quaternion.IDENTITY);
        return transform;
    }


    public Transform translate(Vector t) {
        var transform = new Transform(Translation.add(t), Rotation);
        return transform;
    }


    public Transform inverse() {
        var rotation = Rotation.inverse();
        var translation = rotation.apply(Translation.inverse());
        return new Transform(translation, rotation);
    }


    /**
     * Applies this transform to the Vector v representing a point in space, returning the new transformed vector.
     */
    public Vector applyToPoint(Vector v) {
        return Rotation.apply(v).add(Translation);
        // return Rotation.apply(v.add(Translation));
    }

    /**
     * Applies this transform to the Vector d representing a direction, returning the new transformed direction.
     */
    public Vector applyToDirection(Vector d) {
        // directions need not be translated.
        return Rotation.apply(d);
    }


    @Override
    public String toString() {
        return String.format("p * %s + %s", Rotation, Translation);
    }
}
